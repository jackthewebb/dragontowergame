using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    bool invuln;
    Coroutine invulnCoroutine;

    public int baseHealth = 20;

    public float invulnTime = 1;

    int health;
    HUDcanvas canvas;
    [SerializeField] SpriteRenderer rocketSprite;
    [SerializeField] SpriteRenderer playerSprite;

    SoundManager soundManager;



    void Start()
    {
        soundManager = GetComponent<SoundManager>();

        health = baseHealth;
        Physics2D.IgnoreLayerCollision(3, 10);

        canvas = FindObjectOfType<HUDcanvas>();
        canvas.PlayerHealthBar(health);

    }

    void Update()
    {
        if (transform.position.y < -35)
        {
            transform.position = new Vector3(6, -20, 0);
            TakeDamage(1);
        }
    }

    IEnumerator FlashCharacter()
    {

        rocketSprite.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        playerSprite.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;

        yield return new WaitForSeconds(.15f);

        rocketSprite.maskInteraction = SpriteMaskInteraction.None;
        playerSprite.maskInteraction = SpriteMaskInteraction.None;

        yield return new WaitForSeconds(.15f);


        if (invuln) StartCoroutine(FlashCharacter());

    }

    IEnumerator InvulnFX()
    {
        invuln = true;
         StartCoroutine(FlashCharacter());
        yield return new WaitForSeconds(invulnTime);
        invuln = false;


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EnemyAttack"))
        {
            TakeDamage(1);
            if (other.GetComponent<EnemyProjectile>())
            {
                other.GetComponent<EnemyProjectile>().OnHitPlayer();
            }
        }
    }

    public void TakeDamage(int amount)
    {
        if (invuln) return;

        health -= amount;
        canvas.BossHealthBar(baseHealth, health);
        soundManager.PlayClip("PlayerTakeDamage");


        GetComponent<AudioSource>().Play(0);
        if (invulnCoroutine != null) StopCoroutine(invulnCoroutine);
        invulnCoroutine = StartCoroutine(InvulnFX());


        if (health <= 0)
        {
            GameOver();
        }
    }
    void GameOver()
    {

        canvas.GameOver();
        StartCoroutine(LoadNextLevel());

    }

    IEnumerator LoadNextLevel()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(0);
    }



}
