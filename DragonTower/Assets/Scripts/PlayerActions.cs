using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    public PlayerProjectile projectile;
    public MeleeHit meleeHit;
    public LayerMask whatIsGround;
    Vector2 aimDirection = new Vector2(-1, 0);
    BoxCollider collision;
    float shotCharge;
    bool canShoot = true;
    bool cooldown = false;
    public float shotCooldown = 1;
    float facingDirection = -1;

    bool isPaused;

    [SerializeField] Animator rocketAnim;

    Camera cam;
    public Transform[] BarelPositions;
    Transform shootPosition;

    SoundManager soundManager;
    void Start()
    {
        cam = Camera.main;
        soundManager = GetComponent<SoundManager>();
        collision = GetComponent<BoxCollider>();
        Cursor.visible = false;
        //GetComponentInChildren<MeshRenderer>().material.color = new Color(0, 141, 255, 255);

    }

    public void PauseGame(bool paused) {
        isPaused = paused;

    }

    IEnumerator ShootCooldown()
    {
        cooldown = true;
        yield return new WaitForSeconds(shotCooldown);
        cooldown = false;
    }


    void Update()
    {
        GetShootDirection();



    }

    void GetShootDirection()
    {
        if (isPaused) return;

        float x = Mathf.RoundToInt(Input.GetAxisRaw("Horizontal"));
        float y = Mathf.RoundToInt(Input.GetAxisRaw("Vertical"));
        if (x != 0) facingDirection = x;

        //up
        if (y == 1 && x == 0)
        {
            RotateLauncher(2);
        }
        //up forward
        else if (y == 1 && x != 0)
        {
            RotateLauncher(1);
        }
        //forward
        else if (y == 0)
        {
            RotateLauncher(0);
        }
        //neutral
        if (x == 0 && y <= 0)
        {
            aimDirection = new Vector2(facingDirection, 0);
        }
        else
        {
            
            aimDirection = new Vector2(x, y);
        }


        Shoot();

    }

    void RotateLauncher(int dir)
    {
        rocketAnim.SetInteger("FiringDirection", dir);
        shootPosition = BarelPositions[dir];
    }

    void Shoot()
    {
        if (!canShoot || cooldown) return;

        if (Input.GetButtonDown("Fire2"))
        {
            shotCharge = 1;
            rocketAnim.SetFloat("RocketCharge", 0);
        }

        if (Input.GetButton("Fire2"))
        {
            shotCharge = Mathf.Clamp(shotCharge + Time.deltaTime, 1, 2);
            rocketAnim.SetFloat("RocketCharge", shotCharge - 1);


        }

        if (Input.GetButtonUp("Fire2"))
        {

            PlayerProjectile newProjectile = Instantiate(projectile, shootPosition.position, Quaternion.identity);
            newProjectile.direction = aimDirection.normalized;
            newProjectile.speed *= shotCharge;
            newProjectile.damage *= shotCharge;
            newProjectile.soundManager = soundManager;
            StartCoroutine(ShootCooldown());
            soundManager.PlayClip("PlayerShoot");


            rocketAnim.SetTrigger("Fire");
            rocketAnim.SetFloat("RocketCharge", 0);
            shotCharge = 1;



        }





    }

}
