using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawMinion : MonoBehaviour
{

    ProjectileEgg egg;
    [SerializeField] Animator clawAnim;
    Vector2 direction;
    public int orientaion;

    [SerializeField] float speed;
    Rigidbody2D rb;
    Vector3 target;
    float distance = 100;


    void Start()
    {
        egg = GetComponentInChildren<ProjectileEgg>();
        rb = GetComponent<Rigidbody2D>();

        target = new Vector3(orientaion * distance, transform.position.y, 0);
        StartCoroutine(DropEgg());
    }

    void Update()
    {
        Move();
    }

    IEnumerator DropEgg()
    {

        yield return new WaitForSeconds(3);

        clawAnim.SetTrigger("Open");
        egg.Drop();
        target = new Vector3(orientaion * distance, transform.position.y+distance, 0);
        speed *= 1.5f;
    }

    void Move()
    {

        direction = -(transform.position - target);
        rb.velocity = direction.normalized * speed;

    }




}
