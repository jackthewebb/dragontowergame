using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHit : MonoBehaviour
{
    public float speed;
    public float lifetime;
    public GameObject caster;
    public Vector3 direction;

    private void Start()
    {
        StartCoroutine(Die());
    }

    void Update()
    {
        transform.position = caster.transform.position + direction.normalized * 2;

    }


    IEnumerator Die()
    {
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);
    }
}
