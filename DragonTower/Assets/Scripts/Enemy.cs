using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    HUDcanvas canvas;
    public float health;
    public float baseHealth;
    PlayerMovement player;
    public float attackFrequency = 1f;
    public float speed = 1;
    public float idleSpeed = 1;
    public float rushSpeed = 1;

    SoundManager soundManager;
    Rigidbody2D rb;


    public Transform target;
    public Transform idleA;
    public Transform idleB;
    public Transform offScreenL;
    public Transform offScreenR;
    public Transform lowerMiddle;
    public Transform lowerL;
    public Transform lowerR;
    public Transform ScreenEdgeL;
    public Transform ScreenEdgeR;
    public Transform ScreenEdgeU;
    public Transform groundL;
    public Transform groundR;
    public Transform droneSpawnL;
    public Transform droneSpawnR;
    public Transform OffScreenUpL;
    public Transform OffScreenUpR;


    public ProjectileBomb bomb;
    public GameObject hitboxRush;


    EnemyProjectile projectile;
    public EnemyProjectile bigfire;
    public EnemyProjectile fireBall;
    public EnemyProjectile burstFireball;
    public EnemyProjectile projectileFollow;

    public GameObject fireMinion;
    public Transform fireMinionSpawnerR;
    public Transform fireMinionSpawnerL;
    public Transform shootPos;

    bool randomness2;

    public GameObject EggDrone;
    
    Vector3 direction;

    public ProjectileFloorFire floorFire;
    public GameObject warningFloorFire;

    public EnemyProjectile[] randomProjectiles;

    public float distanceToTarget;

    bool initialPatrol = true;

    public float FireMinionSpawnFrequency = 12f;
    public float bigfireChargeTime = 1.5f;

    bool randomness;
    bool shooting;
    bool rush;


    int phase;

    int facingDirection = 1;

    bool isMovementPaused;

    Animator anim;


    public bool jumpAnim;

    bool canMove;
    bool turning;
    bool phaseTransition;

    int[] phaseMoves = { 0, 0, 2, 4, 1, 0 };
    List<int> attackOrder = new List<int>();
    int moveNum;

    int testingMove = -1;



    Coroutine pauseMovementCoroutine;
    Coroutine idleMovementCoroutine;

    void Start()
    {

        Physics2D.IgnoreLayerCollision(8, 9);
        Physics2D.IgnoreLayerCollision(8, 10);
        Physics2D.IgnoreLayerCollision(8, 11);
        Physics2D.IgnoreLayerCollision(10, 11);
        rb = GetComponent<Rigidbody2D>();
        soundManager = GetComponent<SoundManager>();
        canvas = FindObjectOfType<HUDcanvas>();
        health = baseHealth;
        player = FindObjectOfType<PlayerMovement>();
        anim = GetComponentInChildren<Animator>();

        NextPhase();
        StartCoroutine(SpawnFireMinionLoop());


    }

    void Update()
    {

        if (Input.GetKeyDown("1"))
        {
            testingMove = 0;
        }
        if (Input.GetKeyDown("2"))
        {
            testingMove = 1;

        }
        if (Input.GetKeyDown("3"))
        {
            testingMove = 2;

        }
        if (Input.GetKeyDown("4"))
        {
            testingMove = 3;

        }

        if (Input.GetKeyDown("y"))
        {
            NextPhase();
        }
        
        canMove = !isMovementPaused && !shooting;
        if (!canMove)
        {
            rb.velocity = Vector3.zero;
            return;
        }

     
        MoveToTarget();
        

    }

    void NextPhase()
    {
        if(idleMovementCoroutine!=null)
            StopCoroutine(idleMovementCoroutine);
        StartCoroutine(GoToNextPhase());

    }

    IEnumerator GoToNextPhase()
    {
        phaseTransition = true;
        yield return new WaitUntil(() => shooting == false && rush == false);

        anim.SetBool("Turning", false);
        turning = false;
        StartCoroutine(PauseMovement(1));
        shooting = false;
        rush = false;
        anim.SetBool("Rush", false);


        yield return new WaitForSeconds(1f);
        switch (phase) {
            case 0:
                //STARTING ANIMATION HERE
                anim.SetBool("Laughing", true);
                soundManager.PlayClip("boss_laugh");

                yield return new WaitForSeconds(2f);
                anim.SetBool("Laughing", false);
                NextPhase();

                break;
            case 1:

               
                GetAttackOrder(phase+1);

                idleMovementCoroutine = StartCoroutine(FlyingStance());

                break;

            case 2:
                //TRANSITION;
                GetAttackOrder(phase + 1);

                if (pauseMovementCoroutine != null) StopCoroutine(pauseMovementCoroutine);
                pauseMovementCoroutine = StartCoroutine(PauseMovement(2));
                anim.SetBool("Laughing", true);
                soundManager.PlayClip("boss_laugh");

                yield return new WaitForSeconds(2f);
                anim.SetBool("Laughing", false);

                target = ScreenEdgeU;
                yield return new WaitForSeconds(.4f);

                yield return new WaitUntil(() => distanceToTarget < 1);

                idleMovementCoroutine = StartCoroutine(SideScreenStance());

                break;
            case 3:


                GetAttackOrder(phase + 1);

                if (pauseMovementCoroutine != null) StopCoroutine(pauseMovementCoroutine);
                pauseMovementCoroutine = StartCoroutine(PauseMovement(2));
                anim.SetBool("Laughing", true);
                soundManager.PlayClip("boss_laugh");

                yield return new WaitForSeconds(2f);
                anim.SetBool("Laughing", false);

                target = ScreenEdgeU;

                yield return new WaitForSeconds(.4f);
                yield return new WaitUntil(() => distanceToTarget < 1);

                idleMovementCoroutine = StartCoroutine(StandingStance());

                break;
            case 4:
                //DEATH ANIMATION HERE

                break;
        }
        phase++;
        phaseTransition = false;

    }

    void GetAttackOrder(int pas)
    {
        attackOrder.Clear();
        moveNum = 0;
        for (int i = 0; i < phaseMoves[pas]; i++)
        {
            attackOrder.Add(i);
        }
    }



    /////////////////////////////////////////////////////////////MOVEMENT//////////////////////////////////////////////////////////////////////////////////

    void FaceTarget()
    {
        int previousFacingDirection = facingDirection;
        facingDirection = direction.x > 0 ? -1 : 1;

        if (previousFacingDirection != facingDirection && distanceToTarget > .4f && !turning)
            StartCoroutine(TurnAround(facingDirection));
    }

    IEnumerator TurnAround(int newDirection, bool animate = true)
    {
        if (animate)
        {
            anim.SetBool("Turning", true);

            if (pauseMovementCoroutine != null) StopCoroutine(pauseMovementCoroutine);
            pauseMovementCoroutine = StartCoroutine(PauseMovement(0.4f));
            turning = true;

            yield return new WaitForSeconds(.25f);

            anim.SetBool("Turning", false);
            turning = false;

        }
        transform.localScale = new Vector3(newDirection, transform.localScale.y, transform.localScale.z);
        facingDirection = newDirection;
    }

    public void MoveToTarget()
    {
        if (target == null || !canMove) return;

        distanceToTarget = Vector3.Distance(transform.position, target.position);
        direction = -(transform.position - target.position);

        if (distanceToTarget > 1)
        {
            rb.velocity = direction.normalized * speed;
        }
        else
        {
            rb.velocity = Vector3.zero;
        }
    }

    IEnumerator PauseMovement(float waitTime)
    {
        isMovementPaused = true;
        yield return new WaitForSeconds(waitTime);
        isMovementPaused = false;
    }

    ///////////////////////////////////////////////////////////////////ATTACKS/////////////////////////////////////////////////////////////////////////////////////
    
    public IEnumerator Remote()
    {
        shooting = true;


        anim.SetTrigger("Remote");

        anim.SetBool("Idle", true);
        yield return new WaitForSeconds(3f);

        shooting = false;


    }

    public IEnumerator ShootProjectile()
    {
        soundManager.PlayClip("boss_fire_attack");
        EnemyProjectile newProjectile = Instantiate(projectile, shootPos.position, projectile.transform.rotation);
        newProjectile.player = player.transform;
        yield return new WaitForSeconds(.8f);
        shooting = false;

    }

    public IEnumerator GroundSlam()
    {

        yield return new WaitForSeconds(.5f);
        shooting = false;

    }



    public void SpawnEggDrone()
    {
        Instantiate(EggDrone, droneSpawnR.position, Quaternion.identity);


    }

    IEnumerator RushAttack()
    {

        rush = true;
        anim.SetBool("Rush", true);
        //choose direction
        bool dir = facingDirection == 1;
        Transform positionA = dir ? offScreenL : offScreenR;
        Transform positionB = dir ? offScreenR : offScreenL;

        hitboxRush.SetActive(true);
        //move offscreen then to the other side
        target = positionA;

        if (pauseMovementCoroutine != null) StopCoroutine(pauseMovementCoroutine);
        pauseMovementCoroutine = StartCoroutine(PauseMovement(0.5f));

        StartCoroutine(TurnAround(dir ? -1 : 1, false));

        yield return new WaitUntil(() => distanceToTarget < 1);

        StartCoroutine(TurnAround(dir ? 1 : -1, false));

        speed = rushSpeed;
        target = positionB;

        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => distanceToTarget < 1);

        StartCoroutine(TurnAround(dir ? -1 : 1, false));

        speed = rushSpeed;
        target = positionA;

        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => distanceToTarget < 1);

        anim.SetBool("Rush", false);
        hitboxRush.SetActive(false);

        speed = idleSpeed;
        rush = false;

    }

    IEnumerator BigFireAttack()
    {
        shooting = true;

        anim.SetBool("BigBlow", true);

        yield return new WaitForSeconds(1f);
        anim.SetTrigger("BigBlowFire");

        EnemyProjectile newProjectile = Instantiate(bigfire, transform.position, Quaternion.Euler(0, 0, 240 * facingDirection));
        Animator fireAnim = newProjectile.GetComponentInChildren<Animator>();

        StartCoroutine(CameraShake(bigfire.lifetime, .4f));

        yield return new WaitForSeconds(bigfire.lifetime + 0.2f);

        anim.SetTrigger("BigBlowEnd");
        fireAnim.SetTrigger("End");

        yield return new WaitForSeconds(1.5f);

        anim.SetBool("BigBlow", false);

        shooting = false;

    }

    IEnumerator FloorFireAttack()
    {
        yield return new WaitForSeconds(8f);

        float rando = Random.Range(-9, 9);

        GameObject newWaningProjectile = Instantiate(warningFloorFire, lowerMiddle.position + (Vector3.right * rando), Quaternion.identity);
        yield return new WaitForSeconds(2f);
        Destroy(newWaningProjectile);

        Instantiate(floorFire, lowerMiddle.position + (Vector3.up * floorFire.height) + (Vector3.right * rando), Quaternion.identity);

        StartCoroutine(FloorFireAttack());

    }

    void SpawnFireMinion()
    {
        Transform spawner;

        spawner = randomness2 ? fireMinionSpawnerL : fireMinionSpawnerR;


        FireMinion minion = Instantiate(fireMinion, spawner.position, Quaternion.identity).GetComponent<FireMinion>();
        minion.direction = randomness2 ? 1 : -1;

        randomness2 = !randomness2;
    }

    IEnumerator SpawnFireMinionLoop()
    {




        yield return new WaitForSeconds(FireMinionSpawnFrequency);
        SpawnFireMinion();
        StartCoroutine(SpawnFireMinionLoop());
    }

    ///////////////////////////////////////////////////////////////////STANCES/////////////////////////////////////////////////////////////////////////////////////

    public IEnumerator FlyingStance()
    {

        if (initialPatrol)
        {
            target = randomness ? idleA : idleB;
            randomness = !randomness;
            FaceTarget();
            initialPatrol = false;
        }

        yield return new WaitUntil(()=> distanceToTarget <= 1);

        FaceTarget();
        if (target == idleA) target = idleB;
        else target = idleA;

        //DO ATTACKS HERE
     
        shooting = true;
        
        if(moveNum > attackOrder.Count-1)
        {
            GetAttackOrder(phase);
        }


        switch (attackOrder[moveNum])
        {
            case 0:
                for (int j = 0; j < 2; j++)
                {
                    shooting = true;
                    projectile = burstFireball;
                    anim.SetTrigger("Spit");
                    soundManager.PlayClip("boss_small_fireball");


                    yield return new WaitUntil(() => shooting == false);

                }
                break;
            case 1:

                StartCoroutine(BigFireAttack());
                soundManager.PlayClip("boss_big_beam_fin");


                break;
        }
        moveNum++;


        yield return new WaitUntil(() => shooting == false);
        
        //

        
        yield return new WaitForSeconds(.1f);

        idleMovementCoroutine = StartCoroutine(FlyingStance());

    }


    IEnumerator SideScreenStance()
    {

        float offset = 8f;

        bool dir = randomness;
        randomness = !randomness;

        Transform positionA;
        Transform positionB;
        int LR = 0;

        if (dir)
        {
            LR = 1;
            positionA = ScreenEdgeL;
            positionB = offScreenL;
        }
        else
        {
            LR = -1;
            positionA = ScreenEdgeR;
            positionB = offScreenR;
        }


        transform.position = new Vector3(positionA.position.x - offset * LR, positionA.position.y, 0);
        target = positionA;
        StartCoroutine(TurnAround(LR, false)); ;

        yield return new WaitUntil(() => distanceToTarget < 1);
        yield return new WaitForSeconds(1f);



        //DO ATTACKS HERE     

        if (moveNum > attackOrder.Count - 1)
        {
            GetAttackOrder(phase);
        }

        int moveToUse = attackOrder[moveNum];

        if(testingMove >= 0)
        {
            moveToUse = testingMove;
            testingMove = -1;
        }


        switch (moveToUse)
        {
            case 3:
                StartCoroutine(RushAttack());
                yield return new WaitUntil(() => rush == false);

                break;
            case 1:
                for (int j = 0; j < 3; j++)
                {
                    shooting = true;
                    projectile = burstFireball;
                    anim.SetTrigger("Spit(side)");
                    soundManager.PlayClip("boss_small_fireball");


                    yield return new WaitUntil(() => shooting == false);
                }
                break;
            case 2:
               
                shooting = true;
                projectile = projectileFollow;
                anim.SetTrigger("FollowAttack");
                soundManager.PlayClip("boss_spawn_spooky_flame");


                yield return new WaitUntil(() => shooting == false);
                
                break;
            case 0:

                shooting = true;
                StartCoroutine(Remote());
                yield return new WaitUntil(() => shooting == false);
                break;
        }
        moveNum++;


        target = positionB;

        yield return new WaitForSeconds(2f);

        idleMovementCoroutine = StartCoroutine(SideScreenStance());

    }

    IEnumerator StandingStance()
    {
        speed = rushSpeed;
        bool dir = randomness;
        randomness = !randomness;
        int facing;
        Transform positionA;
        Transform positionB;

        if (dir)
        {
            facing = 1;
            positionA = OffScreenUpL;
            positionB = groundL;
        }
        else
        {
            facing = -1;
            positionA = OffScreenUpR;
            positionB = groundR;
        }

        StartCoroutine(TurnAround(facing, false));
        transform.position = positionA.position;

        target = positionB;
        anim.SetBool("Falling", true);
        yield return new WaitForSeconds(.5f);
        yield return new WaitUntil(() => distanceToTarget < 1);
        anim.SetBool("Falling", false);

        //do attacks here here
        if (moveNum > attackOrder.Count - 1)
        {
            GetAttackOrder(phase);
        }

        switch (attackOrder[moveNum])
        {

            case 0:
                for (int j = 0; j < 3; j++)
                {
                    shooting = true;
                    projectile = fireBall;
                    anim.SetTrigger("Spit");
                    yield return new WaitUntil(() => shooting == false);
                }
                break;
            case 1:

                shooting = true;
                anim.SetTrigger("GroundSlam");
                yield return new WaitUntil(() => shooting == false);
                break;
        }
        moveNum++;


        anim.SetBool("Jumping", true);

        yield return new WaitUntil(() => jumpAnim == true);
        jumpAnim = false;

        target = positionA;

        yield return new WaitForSeconds(.5f);
        yield return new WaitUntil(() => distanceToTarget < 1);
        yield return new WaitForSeconds(1f);
        anim.SetBool("Jumping", false);


        idleMovementCoroutine = StartCoroutine(StandingStance());


    }

    ///////////////////////////////////////////////////////HEALTH THINGS/////////////////////////////////////////////////////////////////////////////

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Projectile"))
        {
            PlayerProjectile projectile = other.GetComponent<PlayerProjectile>();
            projectile.MinusUses();
            TakeDamage(projectile.damage);
        }

        if (other.CompareTag("MeleeHit"))
        {
            TakeDamage(1);
        }
        if (other.CompareTag("EnemyAttack"))
        {
            if (other.GetComponent<EnemyProjectile>().converted)
            {
                TakeDamage(1);
                other.GetComponent<EnemyProjectile>().MinusUses();
            }
        }
    }
    public void TakeDamage(float amount)
    {
        health -= amount;

        if (health <= 0)
        {
            Die();
        }
        else if (health < 134 && phase == 2 && phaseTransition == false)
        {
            NextPhase();
        }
        else if (health < 66 && phase == 3 && phaseTransition == false) 
        {

            NextPhase();

        }

    }

    public void Die()
    {

        canvas.WinGame();
        Destroy(gameObject);

    }

    IEnumerator CameraShake ( float duration, float magnitude)
    {
        Transform cam = Camera.main.transform;

        Vector3 originalPos = cam.localPosition;

        float elapsed = 0.0f;
        
        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            cam.localPosition = new Vector3(x, y, originalPos.z);
            elapsed += Time.deltaTime;

            yield return null;
        }

        cam.localPosition = originalPos;

    }

}
