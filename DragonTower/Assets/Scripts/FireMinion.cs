using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireMinion : MonoBehaviour
{
    public float jumpPower = 1;
    public float jumpFrequency = 1;
    public LayerMask whatIsGround;
    Rigidbody2D rb;
    Animator anim;
    public int direction = 1;
    bool initialJump = true;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine(Jumping());

    }


    IEnumerator Jumping()
    {
        if (initialJump)
        {
            
            rb.AddForce(new Vector2(direction, 2).normalized * jumpPower,ForceMode2D.Force);

            yield return new WaitForSeconds(jumpFrequency * 2);

            initialJump = false;
        }
        yield return new WaitUntil(() => isGrounded());

        anim.SetBool("Jump", true);
        yield return new WaitForSeconds(.3f);


        yield return new WaitUntil(() => !isGrounded());
        yield return new WaitUntil(() => isGrounded());
        anim.SetBool("Jump", false);
        
        yield return new WaitForSeconds(jumpFrequency);
        StartCoroutine(Jumping());

       

    }

    public void Jump()
    {
        rb.AddForce(new Vector2(direction, 2).normalized * jumpPower, ForceMode2D.Force);
    }

    bool isGrounded()
    {
        if (Physics2D.Raycast(transform.position, Vector2.down, 3f, whatIsGround))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
