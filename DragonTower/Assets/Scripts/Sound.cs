using UnityEngine;


[System.Serializable]
public class Sound
{
    public AudioClip soundClip;

    [HideInInspector]
    public AudioSource source;

    [Range(0f, 1f)]
    public float volume;
    
    [Range(-3f, 3f)]
    public float pitch = 1;

    public bool looping;

    public enum SoundType{
    Gameplay,
    Music,
    Ambient,
    }
    public SoundType soundType;

}
