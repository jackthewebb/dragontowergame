using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SoundManager : MonoBehaviour
{
    public Sound[] sounds;
    bool initialised;


    void Start()
    {
   
    }

    private void Initialize()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.pitch = s.pitch;
            s.source.clip = s.soundClip;
            s.source.volume = s.volume;
            s.source.loop = s.looping;
        }
        initialised = true;
    }
    public void PlayClip(string clipName)
    {
        if (!initialised) Initialize();


        foreach (Sound s in sounds)
        {
            if(s.soundClip.name == clipName)
            {
                //s.source.volume = s.volume * settings.masterVolume;              
                s.source.Play();
                
            }
        }
    }

    public void StopClip(string clipName)
    {

        foreach (Sound s in sounds)
        {
            if (s.soundClip.name == clipName)
            {
                s.source.Stop();
            }
        }
    }

    public void SetVolume()
    {

        if (!initialised) Initialize();

        //this probs shouldnt run in update so get around to fixing that!
        foreach (Sound s in sounds)
        {
            switch (s.soundType)
            {

                case Sound.SoundType.Gameplay:
                   // s.source.volume = s.volume * settings.masterVolume;

                    break;
                case Sound.SoundType.Music:

                    //s.source.volume = s.volume * settings.masterVolume * settings.musicVolume;
                    break;
            }          
        }
    }

}


