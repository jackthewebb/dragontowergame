using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 10f;
    public float jumpPower = 10f;
    public float fallingSpeed = 1;
    float crouchMultiplier = 1;
    public float crouchSpeed = 0.3f;
    Vector3 move;
    Rigidbody2D rb;
    Vector2 plusGravity;
    CapsuleCollider2D _collider;
    public float crouchHeight = 3.8f;
    public LayerMask whatIsGround;

    float jumpCharge;
    public float jumpTime= .4f;
    bool isJumping;
    public float jumpMultiplier;
    bool crouching;
    public GameObject sprite;
    bool canMove;
    public float crouchOffset = -.8f;

    float facingDirection = -1;
    public float dashDuration;
    public bool isDashing;
    public float dashSpeed;

    float defColliderSize;
    float defColliderOffset;
    SoundManager soundManager;


    Animator anim;

    bool isPaused;

    public void PauseGame(bool paused)
    {
        isPaused = paused;

    }


    void Start()
    {
        soundManager = GetComponent<SoundManager>();

        rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<CapsuleCollider2D>();
        anim = GetComponentInChildren<Animator>();
        plusGravity = new Vector2(0, -Physics2D.gravity.y);

        defColliderSize = _collider.size.y;
        defColliderOffset = _collider.offset.y;


    }


    IEnumerator Dash()
    {
        isDashing = true;
        canMove = false;
        yield return new WaitForSeconds(dashDuration);
        canMove = true;
        isDashing = false;

    }

    void Update()
    {
        if (isPaused) return;


        move = new Vector2(Mathf.RoundToInt(Input.GetAxisRaw("Horizontal")), rb.velocity.y);

        //stand still while aiming
        if (Input.GetButton("Aim"))
        {
            if (isPlayerGrounded()) rb.velocity = new Vector2(0, rb.velocity.y);
            canMove = false;
            anim.SetBool("Walking", false);

        }
        else
        {
            canMove = true;
        }

        if (move.x != 0)
        {
            facingDirection = move.x;
        }
        transform.localScale = new Vector3(facingDirection < 0 ? -1 : 1, transform.localScale.y, transform.localScale.z);

        if (canMove)
        {
            rb.velocity = new Vector3(move.x * speed * crouchMultiplier, rb.velocity.y);

            if (move.x != 0)
            {

                anim.SetBool("Walking", true);
            }
            else
            {
                anim.SetBool("Walking", false);
         
            }
        }
        
        if (isDashing)
        {
            rb.velocity = new Vector3(facingDirection * dashSpeed, rb.velocity.y);
        }

        if (Input.GetButtonDown("Dash"))
        {
            //if (!isDashing) StartCoroutine(Dash());
        }

        if (Input.GetButtonDown("Jump") && isPlayerGrounded()){


            soundManager.PlayClip("PlayerJump");
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
            isJumping = true;

            jumpCharge = 0;
        }

        if(Input.GetAxisRaw("Vertical") == -1 && Input.GetAxisRaw("Horizontal") == 0)
        {
            crouchMultiplier = crouchSpeed;
            anim.SetBool("Crouching", true);
            _collider.size = new Vector2(_collider.size.x, crouchHeight / 2);
            _collider.offset = new Vector2(0, crouchOffset);


        }
        else
        {
            anim.SetBool("Crouching", false);
            _collider.size = new Vector2(_collider.size.x, defColliderSize);
            _collider.offset = new Vector2(_collider.offset.x, defColliderOffset);


            crouchMultiplier = 1;
        }

        if (Input.GetButtonUp("Jump"))
        {
            isJumping = false;
            jumpCharge = 0;

            if(rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.6f);
            }
        }

        if(rb.velocity.y > 0 && isJumping)
        {
            jumpCharge += Time.deltaTime;


            if (jumpCharge > jumpTime) isJumping = false;


            float t = jumpCharge / jumpTime;
            float currentJumpM = jumpMultiplier;

            if(t > 0.5f)
            {
                currentJumpM = jumpMultiplier * (1 - t);
            }

            rb.velocity += plusGravity * currentJumpM * Time.deltaTime;
        }

        if(rb.velocity.y < 0)
        {
            rb.velocity -= plusGravity * fallingSpeed * Time.deltaTime;
        }
        anim.SetBool("Jumping", !isPlayerGrounded());

    }

    bool isPlayerGrounded()
    {
        if (Physics2D.Raycast(transform.position, Vector2.down, 2f, whatIsGround))
        {
            return true;
        }
        else
        {
            return false;
        }    
    }
}
