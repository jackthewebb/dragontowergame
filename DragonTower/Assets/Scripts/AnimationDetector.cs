using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationDetector : MonoBehaviour
{


    public void FinishTurning()
    {

       // GetComponentInParent<Enemy>().finishedTurning = true;
    }

    public void DetectAnimationEnd()
    {

        Destroy(transform.root.gameObject);
    }

    public void BabyMinionShoot()
    {

        GetComponentInParent<BabyDragonMinon>().ShootProjectile();
    }

    public void FireMinionJump()
    {

        GetComponentInParent<FireMinion>().Jump();
    }

    public void DragonFireball()
    {
        StartCoroutine(GetComponentInParent<Enemy>().ShootProjectile());
        
    }
    public void DragonJumping()
    {
        GetComponentInParent<Enemy>().jumpAnim = true;

    }
    public void DragonSlam()
    {
        StartCoroutine(GetComponentInParent<Enemy>().GroundSlam());
 
    }

    public void EggDrone()
    {

        GetComponentInParent<Enemy>().SpawnEggDrone();

    }

}
