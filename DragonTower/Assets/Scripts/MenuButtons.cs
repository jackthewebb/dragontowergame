using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class MenuButtons : MonoBehaviour
{

    public GameObject playButton;


    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
        {

            EventSystem.current.SetSelectedGameObject(playButton);
        }
    }
    public void BeginGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ExitToMenu()
    {
        SceneManager.LoadScene(0);

    }

    public void ExitToDesktop()
    {
        Application.Quit();
    }

}
