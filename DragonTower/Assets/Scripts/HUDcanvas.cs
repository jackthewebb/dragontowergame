using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class HUDcanvas : MonoBehaviour
{
    public Slider bossHealthBar;
    public Text playerHP;
    public CanvasGroup pauseMenu;
    public GameObject resumeButton;

    public Text title;

    public CanvasGroup winMenu;
    public GameObject retryButton;
    public bool paused;
    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Time.timeScale = 1;
        audioSource.Play(0);

    }

    private void Update()
    {

        if (Input.GetButtonDown("Menu"))
        {
            if (!paused)
                PauseGame();
            else Resume();

        }

        if (!paused) return;
        if (EventSystem.current.currentSelectedGameObject == null)
        {

            EventSystem.current.SetSelectedGameObject(retryButton);
        }




    }

    public void BossHealthBar(float baseHealth, float currentHealth) {

        bossHealthBar.maxValue = baseHealth;
        bossHealthBar.value = currentHealth;

    }

    public void PauseGame()
    {
        paused = true;
        FindObjectOfType<PlayerMovement>().PauseGame(paused);
        FindObjectOfType<PlayerActions>().PauseGame(paused);

        pauseMenu.gameObject.SetActive(true);
        Time.timeScale = 0;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(resumeButton);
        audioSource.Pause();


    }

    public void Resume()
    {
        Time.timeScale = 1;
        paused = false;
        FindObjectOfType<PlayerMovement>().PauseGame(paused);
        FindObjectOfType<PlayerActions>().PauseGame(paused);
        pauseMenu.gameObject.SetActive(false);
        audioSource.Play();


    }

    public void WinGame()
    {
        paused = true;
        FindObjectOfType<PlayerMovement>().PauseGame(paused);
        FindObjectOfType<PlayerActions>().PauseGame(paused);
        title.text = "Win!";

        winMenu.gameObject.SetActive(true);
        Time.timeScale = 0;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(retryButton);
        audioSource.Pause();

    }

    public void LoseGame()
    {
        
        paused = true;
        FindObjectOfType<PlayerMovement>().PauseGame(paused);
        FindObjectOfType<PlayerActions>().PauseGame(paused);
        title.text = "You Died";
        winMenu.gameObject.SetActive(true);
        Time.timeScale = 0;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(retryButton);
        audioSource.Pause();

    }
    public void Retry()
    {

        SceneManager.LoadScene(1);

    }



    public void ExitToMenu()
    {

        SceneManager.LoadScene(0);
    }

    public void PlayerHealthBar(int health)
    {
        playerHP.text = health.ToString();

    }

    public void GameOver()
    {
        LoseGame();

    }
}
