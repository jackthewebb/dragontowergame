using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{
    public Vector3 direction;
    public float speed;
    public float lifetime;
    public int uses = 1;
    public float damage = 1;
    public GameObject explosion;
    public GameObject sprite;
    [HideInInspector] public SoundManager soundManager;

    private void Start()
    {
        StartCoroutine(Die());
    }


    public void MinusUses()
    {
        uses--;
        if(uses<= 0)
        {
            Explode();
        }
    }


    void Update()
    {
        transform.Translate(direction.normalized * speed * Time.deltaTime);
        sprite.transform.rotation = Quaternion.LookRotation(Vector3.forward, -direction);

    }


    public void Explode()
    {
        soundManager.PlayClip("RocketHit");
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);

    }
}
