using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBomb : EnemyProjectile
{
    public override void Move()
    {

        transform.position = Vector3.MoveTowards(transform.position, direction, speed*Time.deltaTime);


    }
    public override void OnDeath()
    {
        
        StartCoroutine(Explode());


    }
    IEnumerator Explode()
    {
        GetComponentInChildren<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(.7f);
        GetComponentInChildren<SpriteRenderer>().color = Color.white;

        GetComponentInChildren<Animator>().SetTrigger("Explode");




    }


}
