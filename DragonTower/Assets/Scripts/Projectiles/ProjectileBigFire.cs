using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBigFire : EnemyProjectile
{
    public override void OnHitPlayer()
    {

    }
    public override void OnHitTerrain()
    {
       
    }
    public override void OnHit(PlayerProjectile hitBy)
    {
        
    }
    public override void OnDeath()
    {
        StartCoroutine(DieDelay());

    }


    IEnumerator DieDelay()
    {
        Destroy(GetComponent<BoxCollider2D>());
       
        yield return new WaitForSeconds(3f);

        Destroy(gameObject);

    }
}
