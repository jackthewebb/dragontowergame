using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileEnemyDirectional : EnemyProjectile
{

    public override void Move()
    {

        transform.Translate(direction.normalized * speed * Time.deltaTime);
        sprite.transform.rotation = Quaternion.LookRotation(Vector3.forward, -direction);

    }



}
