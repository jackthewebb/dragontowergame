using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyDragonProjectile : EnemyProjectile
{

    public override void OnHit(PlayerProjectile hitBy)
    {
        MinusUses();
    }

    public override void OnDeath()
    {
        Destroy(gameObject);
    }

}
