using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyDragonMinon : MonoBehaviour
{
    public float distanceToTarget = 3;
    Vector3 target;
    Vector3 direction;
    Rigidbody2D rb;
    public float speed = 1;
    public float idleTime = 1;
    Player player;
    public EnemyProjectile projectile;

    public float offsetY = 1;
    bool canMove;
    bool isMovementPaused;
    Animator anim;
    SoundManager soundManager;

    bool canShoot = true;


    void Start()
    {
        player = FindObjectOfType<Player>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        soundManager = GetComponent<SoundManager>();

        target = transform.position + transform.up * 2;
        StartCoroutine(FlyPattern());
        anim.SetTrigger("Spit");

    }

    void Update()
    {

        distanceToTarget = Vector3.Distance(transform.position, target);

        canMove = !isMovementPaused;

        if (canMove)
        {
            int x = direction.x > 0 ? 1 : -1;
            if (distanceToTarget > .4f)
                transform.localScale = new Vector3(x, transform.localScale.y, transform.localScale.z);

            direction = -(transform.position - target);
            rb.velocity = direction.normalized * speed;
        }
    }

    IEnumerator FlyPattern()
    {
        float animationTime = .5f;

        yield return new WaitUntil(() => distanceToTarget < .4);
        yield return new WaitForSeconds(idleTime);

        anim.SetTrigger("Spit");

        yield return new WaitForSeconds(animationTime);


        target = transform.position + (transform.up + -transform.right) * 2;

        yield return new WaitUntil(() => distanceToTarget < .4);
        yield return new WaitForSeconds(idleTime);

        target = transform.position + (transform.right) * 4;

        yield return new WaitForSeconds(animationTime);

        yield return new WaitUntil(() => distanceToTarget < .4);
        yield return new WaitForSeconds(idleTime * 2);

        target = transform.position + (-transform.up + -transform.right) * 2;

        if (canShoot)
        {
            anim.SetTrigger("Spit");
            soundManager.PlayClip("baby_dragon_fireball");

        }
        StartCoroutine(FlyPattern());

    }

    IEnumerator PauseMovement(float waitTime)
    {
        isMovementPaused = true;
        yield return new WaitForSeconds(waitTime);
        isMovementPaused = false;
    }

    public void ShootProjectile()
    {
        StartCoroutine(PauseMovement(.5f));
        Vector3 direction = new Vector3(player.transform.position.x, player.transform.position.y * offsetY, transform.position.z) - transform.position;
        EnemyProjectile newProjectile = Instantiate(projectile, transform.position, projectile.transform.rotation);
        newProjectile.direction = direction;
        newProjectile.player = player.transform;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            canShoot = false;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            canShoot = true;

        }
    }
}
