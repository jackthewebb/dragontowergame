using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public Vector2 direction;
    public float speed;
    public float lifetime;
    public EnemyProjectile duplicateProjectile;
    public bool converted;
    public int uses = 1;
    [HideInInspector]public Transform player;
    public bool minion;

    public GameObject sprite; 

    private readonly Vector2[] directions = {Vector2.left, Vector2.right};


    public void Start()
    {
        StartCoroutine(Die());
        if(player)
            direction = new Vector2(player.transform.position.x, player.transform.position.y) - new Vector2(transform.position.x, transform.position.y);


    }

    public void Update()
    {
        Move();

    }

    public virtual void OnHitPlayer()
    {
        OnDeath();
    }

    public virtual void Move()
    {
        if (player == null)
            return;

        transform.Translate(direction.normalized * speed * Time.deltaTime);
        sprite.transform.rotation = Quaternion.LookRotation(Vector3.forward, -direction);

    }

    public void MinusUses()
    {
        uses--;
        if (uses <= 0)
        {
            OnDeath();
        }
    }

    public virtual void OnHit(PlayerProjectile hitBy)
    {

    }

    public virtual void OnHitTerrain()
    {
        OnDeath();

    }

    public virtual void OnMelee()
    {
        direction = -direction;
        converted = true;

    }

    public virtual void OnDeath()
    {
        GetComponentInChildren<Animator>().SetTrigger("Die");
    }

    public IEnumerator Die()
    {
        yield return new WaitForSeconds(lifetime);
        OnDeath();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Projectile"))
        {

            OnHit(other.GetComponent<PlayerProjectile>());
            if(minion)
                other.GetComponent<PlayerProjectile>().MinusUses();
        }
        if (other.CompareTag("MeleeHit"))
        {

            OnMelee();
        }
        if (other.CompareTag("Floor"))
        {

            OnHitTerrain();
        }
    }

    public void ExplodeBurst()
    {
        foreach (Vector2 d in directions)
        {

            EnemyProjectile newProjectile = Instantiate(duplicateProjectile, transform.position, Quaternion.identity);
            newProjectile.speed *= 1.5f;
            newProjectile.direction = d;

        }
    }
}
