using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileEgg : EnemyProjectile
{

    public GameObject babyDragon;
    bool dropped;


    public void Drop()
    {

        dropped = true;
        transform.parent = null;

    }

    public override void OnHitTerrain()
    {

        Instantiate(babyDragon, transform.position, Quaternion.identity);
        Destroy(gameObject);

    }

    public override void OnHitPlayer()
    {
       
    }

    public override void Move()
    {
        if (dropped)
        {
            transform.Translate(Vector2.down * speed * Time.deltaTime);

        }
    }
    public override void OnDeath()
    {
        Destroy(gameObject);
    }
}
