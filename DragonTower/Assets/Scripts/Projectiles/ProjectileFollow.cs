using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFollow : EnemyProjectile
{
    bool kb;

    void Awake()
    {
    }


    public override void Move()
    {
        if (kb) return;
        
        direction = transform.position - player.position;
        transform.Translate(-direction.normalized * speed * Time.deltaTime);
        


    }
    public override void OnDeath()
    {
        GetComponentInChildren<Animator>().SetTrigger("Die");

    }

    public override void OnHit(PlayerProjectile hitBy)
    {
        StartCoroutine(Knockback());
        GetComponent<Rigidbody2D>().AddForce(hitBy.direction * 500, ForceMode2D.Impulse);
    }

    IEnumerator Knockback()
    {
        kb = true;
        yield return new WaitForSeconds(1f);
        kb = false;
    }



}
