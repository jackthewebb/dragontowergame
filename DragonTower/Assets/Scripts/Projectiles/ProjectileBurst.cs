using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBurst : EnemyProjectile
{
    public override void OnHit(PlayerProjectile hitBy)
    {
        ExplodeBurst();
    }
    public override void OnDeath()
    {
        ExplodeBurst();

        base.OnDeath();
    }

}
